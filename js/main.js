//SCROLL TO BLOCK
const anchors = document.querySelectorAll('a[href="#catalog"]');

for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        const blockID = anchor.getAttribute('href');

        document.querySelector('' + blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
}



//POPUP MENU
const popUp = document.querySelector('.popup-window'),
    showPic = document.getElementsByClassName('card-img'),
    close = document.querySelector('.close'),
    showBlock = document.querySelector('.popup-block');

/*FOREACH pic addEvent*/
for (let pic of showPic) {
    pic.addEventListener('click', function (e) {
        e.preventDefault();
        popUp.style.display = 'block';
        showBlock.style.display = 'block';
    });
}

close.addEventListener('click', function () {
    popUp.style.display = 'none';
    showBlock.style.display = 'none';
    // document.body.style.overflow = 'auto';
});



//SLIDER
let prev = document.querySelector('.prev'),
    next = document.querySelector('.next'),
    currentImg = 0,
    imgSrc = document.querySelector('.pop-img'),
    imgUrl = [];

/*PICTURES ARRAY*/
imgUrl.push('assets/lighthouse.png');
imgUrl.push('assets/novorossiysk.png');
imgUrl.push('assets/sea.png');
imgUrl.push('assets/beach.png');

imgSrc.src = imgUrl[currentImg]; //START PIC

next.addEventListener('click', function (e) {
    // currentImg++;
    e.preventDefault();
    imgSrc.src = imgUrl[currentImg];
    if (currentImg < imgUrl.length - 1){
        currentImg++;
    } else {
        currentImg = 0;
    }
});

prev.addEventListener('click', function (e) {
    // currentImg--;
    e.preventDefault();
    imgSrc.src = imgUrl[currentImg];
    if (currentImg === 0){
        currentImg = imgUrl.length - 1;
    } else {
        currentImg--;
    }
});



//HAMBURGER MENU
const hamburger = document.querySelector('.hamburger'),
    dropMenu = hamburger.querySelector('.drop-menu');

hamburger.addEventListener('mouseover', function (e) {
    e.preventDefault();
    dropMenu.style.bottom = -140 + 'px';
});

hamburger.addEventListener('mouseout', function (e) {
    e.preventDefault();
    dropMenu.style.bottom = 100 + 'px';
});
